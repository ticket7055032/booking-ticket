public class seat extends date{
    public static String[][] Seat = {
        { "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10" },
        { "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10" },
        { "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10" },
        { "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10" },
        { "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9", "E10" }
    };

    public static void showSeat() {
        System.out.println("---------------- Available Seats ----------------");
        for (int i = 0; i < Seat.length; i++) {
            for (int j = 0; j < Seat[i].length; j++) {
                System.out.print(Seat[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("-------------------------------------------------");
    }
    public static int countXXSeats() {
        int count = 0;
        for (int i = 0; i < Seat.length; i++) {
            for (int j = 0; j < Seat[i].length; j++) {
                if (Seat[i][j].equals("XX")) {
                    count++;
                }
            }
        }
        return count;
    }
    public static void resetSeats() {
        char currentLetter = 'A';
        for (int i = 0; i < Seat.length; i++) {
            for (int j = 0; j < Seat[i].length; j++) {
                Seat[i][j] = currentLetter + Integer.toString(j + 1);
            }
            currentLetter++;
        }
    }
    public static int countSeats() {
        int totalCount = 0;
    
        for (int i = 0; i < Seat.length; i++) {
            totalCount += Seat[i].length;
        }
    
        return totalCount;
    }
}

