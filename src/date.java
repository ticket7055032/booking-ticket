public class date {

    static String[][] M = { { "1. 12:00", "2. 14:00", "3. 16:00", "4. 18:00", "5. 20:00", "6. 22:00", "7. 00:00" } };

    
    public static void showDate() {
        System.out.println("---------------- Available Dates ----------------");
        for (int O = 1, N = 20; N < 27; O++, N++) {
            System.out.println(O + ". " + N + "/Aug/2020");
        }
        System.out.println("------------------------------------------------");
    
}

    public static void showTime() {
    System.out.println("---------------- Available Times ----------------");
    for (int n = 0; n < M.length; n++) {
        for (int m = 0; m < M[n].length; m++) {
            if (m > 0) {
                System.out.println(); 
            }
            System.out.print(M[n][m]); 
        }
    }
    System.out.println("\n------------------------------------------------");
    }
}