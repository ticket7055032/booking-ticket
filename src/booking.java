import java.util.InputMismatchException;
import java.util.Scanner;

public class booking extends seat {

    static String selectedMovie = "";
    static String selectedSubtitles = "";
    static String selectedDate = "";
    static String selectedTime = "";
    static String selectedSeats = "";
    static Scanner kb = new Scanner(System.in);
    static String select_4;
    static int countSeats;
    static int selectedCount = 0;
    static int xxCount = seat.countXXSeats();

    

    public static void selectMovie() {
        String input = kb.nextLine();
        if (input.matches("[1-3]")) {
            int select_1 = Integer.parseInt(input);
            if (select_1 >= 1 && select_1 <= 3) {
                selectedMovie = select_1 == 1 ? "Elemental" : select_1 == 2 ? "Barbie" : "Meg 2: The Trench";
                goBack1();
            }
        } else {
            movie.showMovie();
            selectMovie();
        }
    }

    public static void goBack1() {
        System.out.println("You confirm that you will take this movie right? : " + selectedMovie);
        System.out.print("Enter 'y' to confirm or 'n' to select again.: ");
        String select_15 = kb.next();
        if (select_15.equals("n")) {
            selectMovie();
        } else if (select_15.equals("y")){
            selectSubtitles();
        } else {
            System.out.println("Invalid input. Please enter 'y' to confirm or 'n' to select again.");
            goBack1();
        }

    }

    public static void selectSubtitles() {
        System.out.println("---------- Select Subtitles ----------");
        System.out.println("1. English");
        System.out.println("2. Thai");
        System.out.print("Please Select Subtitles: ");
        String choice = kb.next();
        
        if (choice.equals("1")) {
            selectedSubtitles = "English";
            goBack4();
        } else if (choice.equals("2")) {
            selectedSubtitles = "Thai";
            goBack4();
        } else {
            System.out.println("Invalid choice. Please select 1 for English or 2 for Thai.");
            selectSubtitles();
        }
    }
    
    public static void goBack4() {
        System.out.println("You have selected subtitles: " + selectedSubtitles);
        System.out.print("Enter 'y' to confirm or 'n' to select again.: ");
        String select_35 = kb.next();
        if (select_35.equals("n")) {
            selectSubtitles();
        } else if (select_35.equals("y")) {
            date.showDate();
            selectDate();
        } else {
            System.out.println("Invalid input. Please enter 'y' to confirm or 'n' to select again.");
            goBack4();
        }
    }

    public static void selectDate() {
        System.out.print("Please Select Date: ");
        String select_2 = kb.next();

        if (select_2.equals("1")) {
            selectedDate = "20/Aug/2020";
            goBack2();
        } else if (select_2.equals("2")) {
            selectedDate = "21/Aug/2020";
            goBack2();
        } else if (select_2.equals("3")) {
            selectedDate = "22/Aug/2020";
            goBack2();
        } else if (select_2.equals("4")) {
            selectedDate = "23/Aug/2020";
            goBack2();
        } else if (select_2.equals("5")) {
            selectedDate = "24/Aug/2020";
            goBack2();
        } else if (select_2.equals("6")) {
            selectedDate = "25/Aug/2020";
            goBack2();
        } else if (select_2.equals("7")) {
            selectedDate = "26/Aug/2020";
            goBack2();
        } else {
            showErrorMessage("Date");
            date.showDate();
            selectDate();
        }
    }

    public static void goBack2() {
        System.out.println("Confirm date: "+selectedDate);
        System.out.print("Enter 'y' to confirm or 'n' to select again.: ");
        String select_25 = kb.next();
        if (select_25.equals("n")) {
            date.showDate();
            selectDate();
        } else if (select_25.equals("y")){
            date.showTime();
            selectTime();
        }

    }

    public static void selectTime() {
        System.out.print("Please Select Time: ");

        String select_3 = kb.next();
        if (select_3.equals("1")) {
            selectedTime = "12:00";
            goBack3();
        } else if (select_3.equals("2")) {
            selectedTime = "14:00";
            goBack3();
        } else if (select_3.equals("3")) {
            selectedTime = "16:00";
            goBack3();
        } else if (select_3.equals("4")) {
            selectedTime = "18:00";
            goBack3();
        } else if (select_3.equals("5")) {
            selectedTime = "20:00";
            goBack3();
        } else if (select_3.equals("6")) {
            selectedTime = "22:00";
            goBack3();
        } else if (select_3.equals("7")) {
            selectedTime = "00:00";
            goBack3();
        } else {
            showErrorMessage("Time");
            date.showTime();
            selectTime();
        }
    }

    public static void goBack3() {
        System.out.println("Confirm time: "+selectedTime);
        System.out.print("Enter 'y' to confirm or 'n' to select again.: ");
        String select_25 = kb.next();
        if (select_25.equals("n")) {
            date.showTime();
            selectTime();
        } else if (select_25.equals("y")){
            selectAndUpdateSeat();
        }

    }

    public static void showErrorMessage(String item) {
        System.out.println("----------------------------------------------");
        System.out.println(" not found. Please Select "+item +" Again ");

    }

    public static void selectSeat(int selectedCount, int countSeats) {
        select_4 = kb.next();
        boolean seatFound = false;
        boolean validSeat = false;
        
        if (select_4.matches("[A-E][1-9]|A10|B10|C10|D10|E10")) {
            validSeat = true;
            
            for (int i = 0; i < Seat.length; i++) {
                for (int j = 0; j < Seat[i].length; j++) {
                    if (select_4.equalsIgnoreCase(Seat[i][j])) {
                        Seat[i][j] = "XX";
                        seatFound = true;
                        selectedSeats += select_4 + " ";
                        
                        if (selectedCount + 1 >= countSeats) {
                            return;
                        }
                    }
                }
            }
        }
        
        if (!validSeat || !seatFound) {
            showErrorMessage("Seat");
            selectedSeats = "";
            select_4 = "";
            xxCount = 0;
            countSeats = 0;
            selectedCount = 0;
            resetSeats();
            selectAndUpdateSeat();
        }
    }
    private static void selectAndUpdateSeat() {
        int countSeats = getNumberOfSeats();
        
        if (countSeats > seat.countSeats()) {
            System.out.println("Not enough available seats. Please select a lower number.");
            kb.nextLine();
            selectedSeats = "";
            select_4 = "";
            xxCount = 0;
            countSeats = 0;
            selectedCount = 0;
            selectAndUpdateSeat();
            return;
        }
        
        showSelectedSeats(countSeats);
        selectAll();
    }
    
    public static int getNumberOfSeats() {
        System.out.println("------------------------------------------------");
        System.out.print("How many seats do you want: ");

        try {
            int xxCount = kb.nextInt();
            kb.nextLine();
            return xxCount;
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter a valid number.");
            kb.nextLine();
            return getNumberOfSeats();
        }
    }

    
    public static void showSelectedSeats(int countSeats) {
        seat.showSeat();
        System.out.print("Please Select Seat: ");        
        while (selectedCount < countSeats) {
            selectSeat(selectedCount, countSeats);
            selectedCount++;
        }
    }

    public static void selectAll() {
        seat.showSeat();
        xxCount = seat.countXXSeats();
        System.out.println("---------- Welcome To The Payment System ----------");
        System.out.println("1 Seat, Price 160 baht");
        System.out.println("Selected Movie: " + selectedMovie);
        System.out.println("Selected subtitles: " + selectedSubtitles);
        System.out.println("Selected Date: " + selectedDate);
        System.out.println("Selected Time: " + selectedTime);
        System.out.println("The Number Of Selected Seats : " + xxCount);
        System.out.println("Selected Seats: " + selectedSeats);
        System.out.println("Total Price : " + (xxCount * 160) + " Baht");
        System.out.println("-----------------------------------------------------");
        System.out.println("You confirm that you will take this seat right?");
        System.out.println("Enter 'y' to confirm or 'n' to select again.: ");
        String select_6 = kb.next();
        if (select_6.equals("y")) {
            pay();
        } else if (select_6.equals("n")) {      
            resetAll();
        } else {
            System.out.println("----------------------------------------------------");
            System.out.println("Please Select Again");
            selectAll();
        }
    }

    public static void pay() {
        int xxCount = seat.countXXSeats();
        int totalCost = xxCount * 160;
        System.out.println("-------------------- Payment --------------------");
        
        do {
            System.out.print("Enter Payment Amount: ");
            String paymentInput = kb.next();
            
            try {
                int paymentAmount = Integer.parseInt(paymentInput);
    
                if (paymentAmount < totalCost) {
                    System.out.println("Insufficient payment. Please provide more.");
                } else {
                    int change = paymentAmount - totalCost;
                    System.out.println("Change: " + change + " Baht");
                    System.out.println("----------------------------------------------------");
                    System.exit(0);
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a valid payment amount.");
            }
        } while (true);
    }

    public static void resetAll() {
        selectedSeats = "";
        select_4 = "";
        xxCount = 0;
        countSeats = 0;
        selectedCount = 0;
        seat.resetSeats();
        selectedMovie = "";
        selectMovie();
    }
}